﻿export { default as PresentsList } from './list'
export { default as PresentPage } from './page'
export { default as EditPresent} from './edit-present'
export { default as NewPresent } from './add-present'