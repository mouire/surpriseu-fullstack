﻿export { default as Home } from './home'
export { default as Anketa } from './anketa'
export { default as NotFound } from './404'